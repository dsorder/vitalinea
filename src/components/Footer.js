import React from 'react'
import logoDanone from '../assets/img/logo-danone.png';
import logoVitalinea from '../assets/img/logo-vitalinea.png';
export default function Footer() {
    return (
        <div>
            <div className="p-lateral ptb22">
                <div className="logosContainer">
                    <img src={logoDanone} alt="" className="logoDanone"/>
                    <div className="footer_text_d">
                    <p>
                        Términos y condiciones| Políticas de privacidad | Aviso de privacidad
                    </p>
                    <p>
                        Danone de México 2017 ©. Todos los derechos reservados 2017
                    </p>
                </div>
                    <img src={logoVitalinea} alt="" className="logoVitaline"/>
                </div>
                <div className="footer_text">
                    <p>
                        Términos y condiciones| Políticas de privacidad | Aviso de privacidad
                    </p>
                    <p>
                        Danone de México 2017 ©. Todos los derechos reservados 2017
                    </p>
                </div>
            </div>
        </div>
    )
}
