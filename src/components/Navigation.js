import React,{useState} from 'react'
import Logo from '../assets/img/img-logo-vitalinea.png';
import facebook from '../assets/img/facebook.png';
import youtube from '../assets/img/youtube.png';
import instagram from '../assets/img/instagram.png';
import MobileMenu from '../components/MobileMenu';
export default function Navigation() {
    const [isVisible, setisVisible] = useState(false)
    const handleVisible = () =>{
        console.log("cerrando");
        setisVisible(!isVisible);
    }
    return (
        <>
        {isVisible && <MobileMenu isVisible={handleVisible}></MobileMenu>}
        <nav className="BG-tpop">
            <div className="flex-nav p-lateral">
                <img src={Logo} alt="Vitalinea Logo"  className="logo"/>
                <div>
                    <ul className="desktop_menu">
                        <div className="menu_desktop_flex">
                            <li className="desktop_menu_items"><a href="#">Nuestros productos</a></li>
                            <li className="desktop_menu_items"><a href="#">Disfruta cuidarte</a></li>
                            <li className="desktop_menu_items"><a href="#">Blog</a></li>
                        </div>
                        <div className="menu_desktop_flex">
                            <li className="sociales_space"><a href="#"><img src={facebook} alt="" className="sociales"/></a></li>
                            <li className="sociales_space"><a href="#"><img src={instagram} alt="" className="sociales"/></a></li>
                            <li className="sociales_space"><a href="#"><img src={youtube} alt="" className="sociales"/></a></li>
                        </div>
                    </ul>
                </div>
                <div className="mobileContainer" onClick={handleVisible}>
                    <div className="mobileBar "></div>
                    <div className="mobileBar mbr"></div>
                    <div className="mobileBar "></div>
                </div>
            </div>
        </nav>
        </>
    )
}
