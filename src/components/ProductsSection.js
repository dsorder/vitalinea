import React, { useRef, useEffect } from "react";
import { gsap } from "gsap";
import { Draggable } from "gsap/Draggable";
import producto1 from '../assets/img/vitalinea1.png';
import producto2 from '../assets/img/vitalinea2.png';
import producto3 from '../assets/img/vitalinea3.png';

import fresa_n from '../assets/img/fresa-normal4.png';
import fresa_b4 from '../assets/img/fresa-blur-4.png';
import fresa_n3 from '../assets/img/fresa-normal3.png';
gsap.registerPlugin(Draggable);
export default function ProductsSection() {

      const sliderRef = useRef(null);
        useEffect(() => {
            Draggable.create(sliderRef.current, {
            type: "x",
            bounds: {
                minX: -sliderRef.current.clientWidth + window.innerWidth * 1,
            }
            });
        }, []);
    return (
        <div className="bgfamilia">
            <img src={fresa_n} alt="" className="pa fresanormalp"/>
            <img src={fresa_b4} alt="" className="pa fresb4"/>
            <img src={fresa_n3} alt="" className="pa fresan3"/>
            <div className="p-lateral container_familia">
                <h1 className="familia-vitalinea">LA FAMILIA VITALÍNEA</h1>
                <div id="slider" className="slider" ref={sliderRef}>
                    <div>
                        {/* <Slide imageSource={producto1} className="vg"/> */}
                        <div className="slide">
                            <div className="preview vg">
                            <img src={producto1} alt="slider" draggable="false"/>
                            </div>
                        </div>
                        <h3 className="productName">Vitalínea Griego</h3>
                    </div>
                    <div>
                        {/* <Slide imageSource={producto2} className="vs"/> */}
                        <div className="slide">
                            <div className="preview vs">
                            <img src={producto2} alt="slider" draggable="false"/>
                            </div>
                        </div>
                        <h3 className="productName">Vitalínea sin Azúcar</h3>
                    </div>
                    <div>
                        {/* <Slide imageSource={producto3} className="vb"/> */}
                        <div className="slide">
                            <div className="preview vb">
                            <img src={producto3} alt="slider" draggable="false"/>
                            </div>
                        </div>
                        <h3 className="productName">Vitalínea Bebible</h3>
                    </div>
                    
                </div>
            </div>
        </div>
    )
}
