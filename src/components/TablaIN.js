import React from 'react'
import fresa from '../assets/img/fresa-normal3.png';
export default function TablaIN() {
    return (
        <div className="table-Container">
            <img src={fresa} alt="" className="fresa-normal-3"/>
              <table className="tabla-Informacion-Nutrimental p-lateral">
                <tr>
                    <td>Contenido energético kJ/kcale</td>
                    <td>413,2/97,9</td>
                </tr>
                <tr>
                    <td>Proteínas (g)</td>
                    <td>5.3</td>
                </tr>
                <tr>
                    <td>Grasas (lípidos) (g)</td>
                    <td>2.1</td>
                </tr>
                <tr>
                    <td>Grasas saturadas (g)</td>
                    <td>1.4</td>
                </tr>
                <tr>
                    <td>Carbohidratos (Hidratos de carbono) (g)	</td>
                    <td>12.4</td>
                </tr>
                <tr>
                    <td>Azúcares (g)	</td>
                    <td>7.7</td>
                </tr>
                <tr>
                    <td>Azúcares añadidos (g) 	</td>
                    <td>0.1</td>
                </tr>
                <tr>
                    <td>Fibra dietética (g)	</td>
                    <td>0.0</td>
                </tr>
                <tr>
                    <td>Sodio (mg)	</td>
                    <td>92.7</td>
                </tr>
                <tr>
                    <td>Calcio (mg)	</td>
                    <td>199.6</td>
                </tr>
                <tr>
                    <td>%VNR*</td>
                    <td>22</td>
                </tr>
            </table>
        </div>
    )
}
