import React from 'react'
import fresablur from '../assets/img/fresa-blur.png';
import fresablur2 from '../assets/img/fresa-normal2.png';
import fresanormal1 from '../assets/img/fresa-normal1.png';
import bote from '../assets/img/vitaline-bote.png';
import selloavalado from '../assets/img/sello-avalado.png';
import selloKcal from '../assets/img/sello-kcal.png';
import flecha from '../assets/img/flecha.svg';
export default function HeroSection() {
    return (
        <header className="BG-mob">
            <img src={fresablur} alt="" className="fresa-blur"/>
            <img src={fresablur2} alt="" className="fresa-blur2"/>
            <div className="Header-container">
            <img src={fresanormal1} alt="" className="fresa-normal-1"/>
                <div className="container_bebible flex-1 container-vit">
                    <img src={bote} alt="" className="fresa-bebible-1-1 flex-1 bounce-in-top"/>
                </div>
                <div className="flex-1 p-lateral-m container-vit">
                    <div className="span-sabores">
                        <span>Sabor:</span>
                        <span className="sabor active">Fresa</span>
                        <span className="sabor">Guayaba</span>
                        <span className="sabor">Toronja</span>
                    </div>
                    <div>
                        <h1 className="Vitalnea-Bebible-F">Vitalínea® Bebible Fresa 217 gr.</h1>
                        <p>¡La presentación ideal para llevar contigo! Vitalínea bebible es la opción si eres de las personas que siempre están activas y quieren probar algo sano, rico y práctico.</p>
                        <button className="buttonComprar">Comprar en Walmart</button>
                    </div>
                    <div className="sellos-container">
                        <img src={selloavalado} alt="" className="sello-avalado sellos"/>
                        <img src={selloKcal} alt="" className="sello-Kcal sellos"/>
                    </div>
                    <div className="containerCircle">
                        <div className="circleButton">
                            <img src={flecha} alt="" className="flecha"/>
                        </div>
                    </div>
                </div>
            </div>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="none">
                <polygon fill="white" points="0,100 100,100 0,0"/>
            </svg>
        </header>
    )
}
