import logo from './logo.svg';
import './App.scss';
import HeroSection from './components/HeroSection';
import Navigation from './components/Navigation';
import SectionIN from './components/SectionIN';
import TablaIN from './components/TablaIN';
import ProductsSection from './components/ProductsSection';
import Footer from './components/Footer';
import fresan from './assets/img/fresa-normal2.png';
function App() {
  return (
    <>
      <Navigation></Navigation>
      <HeroSection></HeroSection>
      <div>
      <h1 className="Informacin-nutrimen title_desktop p-lateral-d slide-in-bck-center">Información nutrimental</h1>
        <div className="flex_row_center pr">
          <img src={fresan} alt="" className="fresan"/>
          <SectionIN></SectionIN>
          <TablaIN></TablaIN>
        </div>
      </div>
      <ProductsSection></ProductsSection>
      <Footer></Footer>
    </>
  );
}

export default App;
