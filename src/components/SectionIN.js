import React from 'react'
import fresa1 from '../assets/img/fresa-blur3.png';
import fresablur2 from '../assets/img/fresa-blur2.png';
export default function SectionIN() {
    return (
        <div className="mb-68 desk_w">
            <div className="p-lateral-m">
                <img src={fresablur2} alt="" className="fresa-blur-2"/>
                <img src={fresa1} alt="" className="fresa-blur-3"/>
                <h1 className="Informacin-nutrimen title_movil">Información nutrimental</h1>
                <p className="Vitalnea-Bebible-G">
                Vitalínea® Bebible Guayaba 217gr <br />
                Valor promedio por porción de 217gr <br />
                Porciones por envase: 1
                </p>
                <p className="Ingredientes-Leche">
                <span className="text-style-1">Ingredientes:</span> <br />
Leche descremada pasteurizada y/o reconstituida pasteurizada de vaca. 3.5% preparado de fruta guayaba (acesulfame K y sucralosa (27.2mg/100g)), crema, almidón modificado, maltodextrina y cultivos lácticos.
                </p>
            </div>
        </div>
    )
}
